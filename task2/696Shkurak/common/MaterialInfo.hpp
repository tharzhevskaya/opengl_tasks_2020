#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
Параметры материала
*/
struct MaterialInfo
{
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess;
};
