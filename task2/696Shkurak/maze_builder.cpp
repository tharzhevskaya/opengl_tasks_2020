#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>
#include <sstream>
#include <vector>

#include <LightInfo.hpp>
#include <MaterialInfo.hpp>
#include <Texture.hpp>
#include "maze_builder_mesh.hpp"

class SampleApplication : public Application {
public:
    SampleApplication() {
        _cameraMover = std::make_shared<FreeCameraMover>();
        _cameraMover2 = std::make_shared<OrbitCameraMover>();
    }
    Maze My_Maze;
    MeshPtr _marker_light_top; 
    MeshPtr _marker_light_spot; 

    ShaderProgramPtr _marker_Shader;

    ShaderProgramPtr _light_spot_Shader;
    ShaderProgramPtr _light_top_Shader;
    ShaderProgramPtr _shader = _light_spot_Shader;

    bool _is_top_view = false;

    /* Создаем переменные для управления положением одного источника света */
    float _lr = 10.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.5f;

    LightInfo _top_light;
    LightInfo _head_light;
    LightInfo _light = _head_light;

    TexturePtr _wall_tex;
    TexturePtr _wall_tex_nm;
    TexturePtr _floor_tex;
    TexturePtr _top_tex;

    MaterialInfo _wall_mat;
    MaterialInfo _floor_mat;
    MaterialInfo _top_mat;
    MaterialInfo _poster_mat;

    std::vector<TexturePtr> _maze_posters;
    GLuint _sampler;
    GLuint _sampler_posters;

    void makeScene() override
    {
        Application::makeScene();

        /* Создаем и загружаем меши */

        My_Maze = makeLabirint("696ShkurakData2/maze_structure.txt");
        My_Maze.Wall->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        My_Maze.Floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        My_Maze.Ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

        for (int i = 0; i < My_Maze.Posters_cnt; i++) {
            My_Maze.Poster_array[i]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        }
        _marker_light_top = makeSphere(0.5f);
        _marker_light_spot = makeSphere(0.01f);
        
        /* Зададим значения переменных для освщения */

        _top_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _top_light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _top_light.diffuse = glm::vec3(0.5, 0.5, 0.5);
        _top_light.specular = glm::vec3(1.0, 1.0, 1.0);


        _top_light.attenuation0 = 1.0;
        _top_light.attenuation1 = 0.02;
        _top_light.attenuation2 = 0.005;

        _head_light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _head_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _head_light.specular = glm::vec3(1.0, 1.0, 1.0);
        _head_light.attenuation0 = 1.0;
        _head_light.attenuation1 = 0.02;
        _head_light.attenuation2 = 0.005;

        /* Инициализируем шейдеры */

        _light_top_Shader = std::make_shared<ShaderProgram>("696ShkurakData2/shaders/marker_top.vert", "696ShkurakData2/shaders/marker_top.frag");
        _light_spot_Shader = std::make_shared<ShaderProgram>("696ShkurakData2/shaders/marker_spot.vert", "696ShkurakData2/shaders/marker_spot.frag");
        _marker_Shader = std::make_shared<ShaderProgram>("696ShkurakData2/shaders/marker.vert", "696ShkurakData2/shaders/marker.frag");

        /* Задаем значения переменных материалов */

        _wall_mat.ambient = glm::vec3(0.8, 0.8, 0.8);
        _wall_mat.diffuse = glm::vec3(1.0, 1.0, 1.0);
        _wall_mat.specular = glm::vec3(0.1, 0.1, 0.1);
        _wall_mat.shininess = 16.0;
        
        _floor_mat.ambient = glm::vec3(0.8, 0.8, 0.8);
        _floor_mat.diffuse = glm::vec3(1.0, 1.0, 1.0);
        _floor_mat.specular = glm::vec3(0.6, 0.6, 0.6);
        _floor_mat.shininess = 32.0;

        _top_mat.ambient = glm::vec3(0.8, 0.8, 0.8);
        _top_mat.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _top_mat.specular = glm::vec3(0.6, 0.6, 0.6);
        _top_mat.shininess = 64.0;

        _poster_mat.ambient = glm::vec3(0.8, 0.8, 0.8);
        _poster_mat.diffuse = glm::vec3(1.0, 1.0, 1.0);
        _poster_mat.specular = glm::vec3(0.1, 0.1, 0.1);
        _poster_mat.shininess = 128.0;

        /* Загружаем пол, потолок, стену и карту нормалей для стены */

        _floor_tex = loadTexture("696ShkurakData2/images/floor.jpg");
        _top_tex = loadTexture("696ShkurakData2/images/ceiling.jpg");

        _wall_tex = loadTexture("696ShkurakData2/images/wall.jpg");
        _wall_tex_nm = loadTexture("696ShkurakData2/images/normal_mapped_wall.jpg");

        /* Добавим постеры */

        std::string poster_addr1 = "696ShkurakData2/images/poster_art2.jpg";
        std::string poster_addr2 = "696ShkurakData2/images/poster_art3.jpg";
        std::string poster_addr3 = "696ShkurakData2/images/poster_art1.jpg";

        _maze_posters.push_back(loadTexture(poster_addr1));
        _maze_posters.push_back(loadTexture(poster_addr2));
        _maze_posters.push_back(loadTexture(poster_addr3));

        /* Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры */
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 8.0f);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
            ImGui::RadioButton("top_view", reinterpret_cast<int*>(&_is_top_view), true);
            ImGui::RadioButton("not_top_view", reinterpret_cast<int*>(&_is_top_view), false);
        }
        ImGui::End();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_Z) {
                _is_top_view = !_is_top_view;
                _cameraMover2.swap(_cameraMover);
            }
        }
    }

    void draw() override
    {
        /* Получаем текущие размеры экрана и выставлям вьюпорт */
        int width, height; 
        GLuint textureUnitForDiffuseTex1 = 0;
        GLuint textureUnitForDiffuseTex1_nm = 1;
        GLuint textureUnitForDiffuseTex2 = 2;
        GLuint textureUnitForDiffuseTex3 = 3;
        GLuint textureUnitForPosters = 4;


        glm::vec3 lightPosCamSpace;
        _top_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _head_light.position = std::static_pointer_cast<FreeCameraMover>(_cameraMover)->getPosition() + glm::vec3(0.0, 0.0, 0.25);
        if (_is_top_view) {
            _light = _top_light;
            _shader = _light_top_Shader;
        }
        else {
            _light = _head_light;
            _shader = _light_spot_Shader;
        }
        lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); 
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);
        _shader->setVec3Uniform("light.a", glm::vec3(_light.attenuation0, _light.attenuation1, _light.attenuation2));

        
        glBindSampler(textureUnitForDiffuseTex1, _sampler);
        glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex1); 
        _wall_tex->bind();
        glBindSampler(textureUnitForDiffuseTex1_nm, _sampler);
        glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex1_nm); 
        _wall_tex_nm->bind();
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex1);
        _shader->setIntUniform("normalMap", textureUnitForDiffuseTex1_nm);
        _shader->setVec3Uniform("material.Ka", _wall_mat.ambient);
        _shader->setVec3Uniform("material.Kd", _wall_mat.diffuse);
        _shader->setVec3Uniform("material.Ks", _wall_mat.specular);
        _shader->setFloatUniform("material.shininess", _wall_mat.shininess);
        _shader->setIntUniform("material.haveNormalMap", true);
        {
            _shader->setMat4Uniform("modelMatrix", My_Maze.Wall->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * My_Maze.Wall->modelMatrix()))));

            My_Maze.Wall->draw();
        }
        

        glBindSampler(textureUnitForDiffuseTex2, _sampler);
        glActiveTexture(GL_TEXTURE0+ textureUnitForDiffuseTex2);
        _floor_tex->bind();
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex2);
        _shader->setVec3Uniform("material.Ka", _floor_mat.ambient);
        _shader->setVec3Uniform("material.Kd", _floor_mat.diffuse);
        _shader->setVec3Uniform("material.Ks", _floor_mat.specular);
        _shader->setFloatUniform("material.shininess", _floor_mat.shininess);
        _shader->setIntUniform("material.haveNormalMap", false);

        {
            _shader->setMat4Uniform("modelMatrix", My_Maze.Floor->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * My_Maze.Floor->modelMatrix()))));

            My_Maze.Floor->draw();
        }

        if (!_is_top_view) {
            glBindSampler(textureUnitForDiffuseTex2, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex2);
            _top_tex->bind();
            _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex2);
            _shader->setVec3Uniform("material.Ka", _top_mat.ambient);
            _shader->setVec3Uniform("material.Kd", _top_mat.diffuse);
            _shader->setVec3Uniform("material.Ks", _top_mat.specular);
            _shader->setFloatUniform("material.shininess", _top_mat.shininess);
            _shader->setIntUniform("material.haveNormalMap", false);

            {
                _shader->setMat4Uniform("modelMatrix", My_Maze.Ceiling->modelMatrix());
                _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * My_Maze.Ceiling->modelMatrix()))));

                My_Maze.Ceiling->draw();
            }
        }

        for (int i = 0; i < My_Maze.Posters_cnt; ++i) {
            {
                glBindSampler(textureUnitForPosters + i, _sampler);
                glActiveTexture(GL_TEXTURE0 + textureUnitForPosters + i);  //текстурный юнит 3+i
                _maze_posters[i]->bind();
                _shader->setIntUniform("diffuseTex", textureUnitForPosters + i);
                _shader->setVec3Uniform("material.Ka", _poster_mat.ambient);
                _shader->setVec3Uniform("material.Kd", _poster_mat.diffuse);
                _shader->setVec3Uniform("material.Ks", _poster_mat.specular);
                _shader->setFloatUniform("material.shininess", _poster_mat.shininess);
                _shader->setIntUniform("material.haveNormalMap", false);

                {
                    _shader->setMat4Uniform("modelMatrix", My_Maze.Poster_array[i]->modelMatrix());
                    _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * My_Maze.Poster_array[i]->modelMatrix()))));

                    My_Maze.Poster_array[i]->draw();
                }
            }
        }

        /* Для обоих источников света рисуем маркеты */
        if(_is_top_view) {
            _marker_Shader->use();
            _marker_Shader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _marker_Shader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker_light_top->draw();
        }

        /* Отсоединяем сэмплер и шейдерную программу */
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    /* system("pause"); */
    return 0;
}