#version 330

/** 
Берем из презентации и примеров шейдеры освещения: 
*/

uniform sampler2D diffuseTex;
uniform sampler2D normalMap;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
	vec3 a;
};

struct MaterialInfo
{
	vec3 Ka; //Коэффициент отражения окружающего света
	vec3 Kd; //Коэффициент отражения диффузного света
	vec3 Ks; //Коэффициент бликового отражения
	float shininess;
	bool haveNormalMap;
};

uniform LightInfo light;
uniform MaterialInfo material;

in mat3 tbn;
/**
координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
*/
in vec4 posCamSpace; 

/**
текстурные координаты (интерполирована между вершинами треугольника)
*/
in vec2 texCoord;
out vec4 fragColor;

/**
Задаю  значение cos^2 угла света (лобовой свет)
*/
const float lite_angle = 0.98;


void main()
{
	vec3 Norm;
	vec3 color = texture(diffuseTex, texCoord).rgb * material.Ka * light.La;

	vec3 Light_direction_camera = normalize(light.pos - posCamSpace.xyz); //направление на источник света	
	
	/**
	Направление света фонарика
	*/
	vec3 Light_direction = vec3(0, -0.1, -1);

	float lightDirLen2 = pow(Light_direction.x, 2) + pow(Light_direction.y, 2) + pow(Light_direction.z, 2);
	
	float curr_cos2 = pow(dot(Light_direction, Light_direction_camera), 2) / (lightDirLen2 * pow(Light_direction_camera.x, 2) + pow(Light_direction_camera.y, 2) + pow(Light_direction_camera.z, 2));
	
	if (curr_cos2 >= lite_angle) {

		if (material.haveNormalMap) {
			vec3 normalFromTex = normalize(2.0 * (texture(normalMap, texCoord).rgb) - 1.0);
			Norm = normalize(tbn * normalFromTex);
		} 
		else 
		{
			Norm = normalize(tbn * vec3(0, 0, 1));
		}
		
		color += texture(diffuseTex, texCoord).rgb * material.Kd*light.Ld * max(dot(Norm, Light_direction_camera.xyz), 0.0);

		if (max(dot(Norm, Light_direction_camera.xyz), 0.0) > 0.0)
		{	
		    /**
			Найдем вектор-биссектрису между направлениями normalize(-posCamSpace.xyz) - вектор направления на камеру и между вектором на источник света
			*/
			vec3 halfVector = normalize(Light_direction_camera.xyz + normalize(-posCamSpace.xyz));

			/**
			Используем схему Блинна, настраиваем размер бликов и интенсивность освещения:
			*/
			float blinnTerm = max(dot(Norm, halfVector), 0.0);				
			blinnTerm = pow(blinnTerm, material.shininess); 
			color += material.Ks*light.Ls * blinnTerm;
		}
	}
	fragColor = vec4(color, 1.0);
}
