/**
Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
Копирует на выход текстурные координаты.
*/

#version 330

//стандартные матрицы для преобразования координат

uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform bool haveNormalMap;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitangent;   

out mat3 tbn;

out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec2 texCoord; //текстурные координаты



void main()
{
	texCoord = vertexTexCoord;

	posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
	tbn = mat3(normalize(normalToCameraMatrix * aTangent), normalize(normalToCameraMatrix * aBitangent), normalize(normalToCameraMatrix * vertexNormal));
	
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
