#include <Mesh.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <assert.h>

struct Maze {
	Maze() {}
	~Maze() {}

	MeshPtr Wall;
	MeshPtr Floor;
	MeshPtr Ceiling;
	size_t Posters_cnt = 0;
	std::vector<MeshPtr> Poster_array;
};

void read_data(std::ifstream& f, MeshPtr& mesh) {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;

	int N = 0;
	float coord_1, coord_2, coord_3;

	f >> N;
	for (size_t i = 0; i < N; i++)
	{
		f >> coord_1 >> coord_2 >> coord_3;
		if (i % 3 == 0)
		{
			vertices.push_back(glm::vec3(coord_1, coord_2, coord_3));
		}
		if (i % 3 == 1)
		{
			normals.push_back(glm::vec3(coord_1, coord_2, coord_3));
		}
		if (i % 3 == 2)
		{
			texcoords.push_back(glm::vec2(coord_1, coord_2));
		}
			
	}

	assert(vertices.size() == texcoords.size());
	assert(vertices.size() == normals.size());

	for (size_t i = 0; i < vertices.size(); i++) {
		if (i % 3 == 2) {
			/* 
			���������� ������, T��������� ����������, ������ �������
			*/
			glm::vec3 pos1 = vertices[i - 2];
			glm::vec3 pos2 = vertices[i - 1];
			glm::vec3 pos3 = vertices[i - 0];

			glm::vec2 uv1 = texcoords[i - 2];
			glm::vec2 uv2 = texcoords[i - 1];
			glm::vec2 uv3 = texcoords[i - 0];

			glm::vec3 nm = normals[i - 0];
			assert(normals[i - 0] == normals[i - 2]);
			assert(normals[i - 0] == normals[i - 1]);

			glm::vec3 edge1 = pos2 - pos1;
			glm::vec3 edge2 = pos3 - pos1;
			glm::vec2 deltaUV1 = uv2 - uv1;
			glm::vec2 deltaUV2 = uv3 - uv1;

			float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

			float x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
			float y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
			float z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
			glm::vec3 tangent = glm::normalize(glm::vec3(x, y, z));

			x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
			y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
			z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
			glm::vec3 bitangent = glm::normalize(glm::vec3(x, y, z));

			for (size_t j = 0; j < 3; j++)
			{
				tangents.push_back(tangent);
				bitangents.push_back(bitangent);
			}
		}
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf3->setData(tangents.size() * sizeof(float) * 3, tangents.data());

	DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf4->setData(bitangents.size() * sizeof(float) * 3, bitangents.data());

	mesh = std::make_shared<Mesh>();

	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
	mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());
}


Maze makeLabirint(std::string filename) {

	std::ifstream f;
	f.open(filename);
	Maze My_Maze;
	
	/*
	�� ������� ��������� �� ����� � ��������� �����, ���, ������� � �������
	*/
	read_data(f, My_Maze.Wall);
	read_data(f, My_Maze.Floor);
	read_data(f, My_Maze.Ceiling);
	f >> My_Maze.Posters_cnt;
	for (size_t i = 0; i < My_Maze.Posters_cnt; i++)
	{
		My_Maze.Poster_array.push_back(std::make_shared<Mesh>());
		read_data(f, My_Maze.Poster_array[i]);
	}
	return My_Maze;
}