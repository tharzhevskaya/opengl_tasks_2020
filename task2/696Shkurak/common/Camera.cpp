#include <Camera.hpp>
#include <imgui.h>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

void OrbitCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) { }

void OrbitCameraMover::showOrientationParametersImgui()  {
    ImGui::Text("r = %.2f, phi = %.2f, theta = %2f", _r, _phiAng, _thetaAng);
}

void OrbitCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        _phiAng = _phiAng - (dx * 0.005);
        _thetaAng = _thetaAng + (dy * 0.005);

        _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.49, glm::pi<double>() * 0.49);
    }
    _oldXPos = xpos;
    _oldYPos = ypos;
}

void OrbitCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset) {
    _r = _r + (yoffset * _r  * 0.05);
}

/* Камера "вид сверху": начальное положение */

void OrbitCameraMover::update(GLFWwindow* window, double dt)
{
    double speed = 2.0; /* Скорость перемещения */

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        _phiAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        _phiAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        _thetaAng += speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        _thetaAng -= speed * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
        _r += _r * dt;
    }
    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) {
        _r -= _r * dt;
    }

    _thetaAng = glm::clamp(_thetaAng, -glm::pi<double>() * 0.5, glm::pi<double>() * 0.5);

    glm::vec3 pos = glm::vec3(
        glm::cos(_phiAng) * glm::cos(_thetaAng), 
        glm::sin(_phiAng) * glm::cos(_thetaAng), 
        glm::sin(_thetaAng) + 0.5f) * (float)_r;

    /* 
    В библиотеке GLM есть удобная функция glm::lookAt для построения матрицы вида. Она принимает на вход 3 вектора:
    - вектор eye - соединяет начало мировой системы координат и виртуальную камеру
    - вектор center - соединяет начало мировой системы координат и точку, на которую смотрит камера
    - вектор up - вектор «вверх», обычно берется(0, 0, 1) или(0, 1, 0), смотря какая ось у вас направлена вверх в мировой системе координат
    */
    _camera.viewMatrix = glm::lookAt(pos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));


    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    /* Обновляем матрицу проекции, если размеры окна изменятся */

    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

/* Камера "переднего вида": начальная позиция у входа в лабиринт */

FreeCameraMover::FreeCameraMover() : CameraMover(), _pos(0.5f, -7.0f, 1.4f) {       
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.01f, 24.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void FreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) { }

void FreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos) {
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз        
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворот вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void FreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void FreeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 2.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;
    forwDir.z = 0.0;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
    rightDir.z = 0.0;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);


    //Обновляем матрицу проекции на случай, если размеры окна изменились
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

glm::vec3 FreeCameraMover::getPosition()
{
    return _pos;
}
