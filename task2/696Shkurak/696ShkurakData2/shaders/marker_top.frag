#version 330

/**
Из презентации
*/

uniform sampler2D diffuseTex;
uniform sampler2D normalMap;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
	vec3 a;
};

struct MaterialInfo
{
	vec3 Ka; //Коэффициент отражения окружающего света
	vec3 Kd; //Коэффициент отражения диффузного света
	vec3 Ks; //Коэффициент бликового отражения
	float shininess;
	bool haveNormalMap;
};


uniform LightInfo light;
uniform MaterialInfo material;

in mat3 tbn;
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const float sqr_max_ang_cos = 0.85;
void main()
{
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;

	vec3 normal = normalize(tbn*vec3(0,0,1));
	/**
	Направление на источник света:
	*/
	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);
	vec3 color = diffuseColor * (light.La + light.Ld * max(dot(normal, lightDirCamSpace.xyz), 0.0));

	if (max(dot(normal, lightDirCamSpace.xyz), 0.0) > 0.0)
	{			
	    /**
	    Найдем вектор-биссектрису между направлениями normalize(-posCamSpace.xyz) - вектор направления на камеру и между вектором на источник света
	    */
		vec3 halfVector = normalize(lightDirCamSpace.xyz + normalize(-posCamSpace.xyz)); //биссектриса между направлениями на камеру и на источник света

		/**
	    Используем схему Блинна, настраиваем размер бликов и интенсивность освещения:
		*/
		float blinnTerm = max(dot(normal, halfVector), 0.0); 		
		color += light.Ls * pow(blinnTerm, material.shininess) * material.Ks;
	}

	fragColor = vec4(color, 1.0);
}
